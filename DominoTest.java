package base;

import static org.junit.Assert.*;
import org.junit.Test;

public class DominoTest {

    @Test
    public void testPlace() {
        Domino domino = new Domino(3, 5);
        DominoPlacement placement = new DominoPlacement(1, 2, 3, 4);
        domino.place(placement);


        assertEquals(1, domino.hx);
        assertEquals(2, domino.hy);
        assertEquals(3, domino.lx);
        assertEquals(4, domino.ly);
        assertTrue(domino.placed);
    }


}
