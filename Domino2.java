	package base;
	/**
	 * @author Kevan Buckley, maintained by __student
	 * @version 2.0, 2014
	 */
	
	public class Domino implements Comparable<Domino> {
	  public static class DominoData {
			public int high;
			public int low;
			public int hx;
			public int hy;
			public int lx;
			public int ly;
			public boolean placed;

			public DominoData(boolean placed) {
				this.placed = placed;
			}
		}


	public DominoData data = new DominoData(false);

	public Domino(int high, int low) {
	    super();
	    this.data.high = high;
	    this.data.low = low;
	  }
	  
	  public void place(int hx, int hy, int lx, int ly) {
	    this.data.hx = hx;
	    this.data.hy = hy;
	    this.data.lx = lx;
	    this.data.ly = ly;
	    data.placed = true;
	  }
	
	  public String toString() {
	    StringBuffer result = new StringBuffer();
	    result.append("[");
	    result.append(Integer.toString(data.high));
	    result.append(Integer.toString(data.low));
	    result.append("]");
	    if(!data.placed){
	      result.append("unplaced");
	    } else {
	      result.append("(");
	      result.append(Integer.toString(data.hx+1));
	      result.append(",");
	      result.append(Integer.toString(data.hy+1));
	      result.append(")");
	      result.append("(");
	      result.append(Integer.toString(data.lx+1));
	      result.append(",");
	      result.append(Integer.toString(data.ly+1));
	      result.append(")");
	    }
	    return result.toString();
	  }
	
	  /** turn the domino around 180 degrees clockwise*/
	  
	  public void invert() {
	    int tx = data.hx;
	    data.hx = data.lx;
	    data.lx = tx;
	    
	    int ty = data.hy;
	    data.hy = data.ly;
	    data.ly = ty;    
	  }
	
	  public boolean ishl() {    
	    return data.hy==data.ly;
	  }
	
	
	  public int compareTo(Domino arg0) {
	    if(this.data.high < arg0.data.high){
	      return 1;
	    }
	    return this.data.low - arg0.data.low;
	  }

	public void place(position highPosition, position lowPosition) {
		// TODO Auto-generated method stub
		
	}

	public String isHorizontal() {
		// TODO Auto-generated method stub
		return null;
	}
	  
	  
	  
	}
