package base;

import static org.junit.Assert.*;
import java.awt.*;
import org.junit.Test;

public class PictureFrameTest {

    @Test
    public void testGetPreferredSize() {
        // Create an instance of PictureFrame
        PictureFrame pictureFrame = new PictureFrame();
        
        // Call the method to be tested
        Dimension preferredSize = pictureFrame.getPreferredSize();
        
        // Assertions to verify the behavior of getPreferredSize()
        assertNotNull(preferredSize);
        assertEquals(202, preferredSize.width);
        assertEquals(182, preferredSize.height);
    }
}
