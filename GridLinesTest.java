package base;

import javax.swing.*;
import java.awt.*;
import org.junit.*;

public class GridLinesTest {

    @Test
    public void testDrawGridLines() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(220, 200);

        // Create an instance of the Location class
        Location location = new Location(0, 0);

        // Create a panel to draw the grid lines
        JPanel panel = new JPanel() {
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                // Call the drawGridLines method within a graphical context
                location.drawGridLines(g);
            }
        };

        frame.add(panel);
        frame.setVisible(true);

    }
}
