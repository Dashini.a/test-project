package base;

public class position {
    private int x;
    private int y;

    public position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(" + (x + 1) + "," + (y + 1) + ")";
    }
}
