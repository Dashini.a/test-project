package base;

import static org.junit.Assert.*;
import org.junit.Test;
import java.net.InetAddress;

public class GameConnectionTest {

    @Test
    public void testGameConnectionMethods() {
        // Create an InetAddress object for testing purposes
        InetAddress address = InetAddress.getLoopbackAddress();

        // Create an instance of GameConnection
        GameConnection connection = new ConnectionGenius(address);

        // Test the methods
        connection.fireUpGame();
        
    }
}
