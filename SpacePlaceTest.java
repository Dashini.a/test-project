package base;

import static org.junit.Assert.*;
import org.junit.Test;

public class SpacePlaceTest {

    @Test
    public void testDefaultConstructor() {
        // Arrange
        SpacePlace spacePlace = new SpacePlace();

        // Act
        int xOrg = spacePlace.getxOrg();
        int yOrg = spacePlace.getyOrg();

        // Assert
        assertEquals(0, xOrg);
        assertEquals(0, yOrg);
    }
}

