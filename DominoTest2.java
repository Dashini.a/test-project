package base;

public class DominoTest2 {
    public static void main(String[] args) {
        // Create a Domino
        Domino domino = new Domino(3, 5);

        // Test placing the Domino
        position highPosition = new position(0, 0);
        position lowPosition = new position(1, 1);
        domino.place(highPosition, lowPosition);

        // Test toString after placing the Domino
        System.out.println("Domino after placing: " + domino);

        // Test inverting the Domino
        domino.invert();
        System.out.println("Domino after inverting: " + domino);

        // Test isHorizontal method
        System.out.println("Is Domino horizontal? " + domino.isHorizontal());

        // Test compareTo method
        Domino otherDomino = new Domino(3, 6);
        int comparisonResult = domino.compareTo(otherDomino);
        if (comparisonResult > 0) {
            System.out.println("Domino is greater than otherDomino");
        } else if (comparisonResult < 0) {
            System.out.println("Domino is smaller than otherDomino");
        } else {
            System.out.println("Domino is equal to otherDomino");
        }
    }
}
