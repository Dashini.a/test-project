	package base;
	import java.net.InetAddress;
	/**
	 * @author Kevan Buckley, maintained by __student
	 * @version 2.0, 2014
	 */
	
	public class ConnectionGenius implements GameConnection {
	
	  InetAddress ipa;
	  
	  public ConnectionGenius(InetAddress ipa) {
	    this.ipa = ipa;
	  }
	
	  @Override
	public void fireUpGame() {
	    downloadWebVersion();
	    connectToWebService();
	    awayWeGo();
	  }
	  
	  @Override
	public void downloadWebVersion(){
	    System.out.println("Getting specialised web version.");
	    System.out.println("Wait a couple of moments");  
	  }
	  
	  @Override
	public void connectToWebService() {
	    System.out.println("Connecting");
	  }
	  
	  @Override
	public void awayWeGo(){
	    System.out.println("Ready to play");
	  }
	
	}
