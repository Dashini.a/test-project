package base;

import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;

import static org.junit.Assert.assertEquals;

public class PictureFrameTest2 {

    @Test
    public void testDrawGridCell() {
        PictureFrame pictureFrame = new PictureFrame();

        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = image.createGraphics();

        int cellX = 10;
        int cellY = 10;
        int cellSize = 20;
        int cellValue = 5;

        pictureFrame.drawGridCell(g2d, cellX, cellY, cellSize, cellValue);

        // Validate the drawn grid cell on the image
        // Example assertions:
        // For instance, you can check if the grid cell has been drawn at the expected coordinates with the expected value
        // This might involve checking colors, shapes, or any specific characteristics of the drawn cell
        // You can use BufferedImage.getRGB() to get the color at specific coordinates and verify it
        // Example:
        // assertEquals("Color at cellX, cellY is as expected", expectedColor.getRGB(), image.getRGB(cellX, cellY));
        // ...
    }
}
