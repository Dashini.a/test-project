package base;

public interface GameConnection {

	void fireUpGame();

	void downloadWebVersion();

	void connectToWebService();

	void awayWeGo();

}