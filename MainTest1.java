package base;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MainTest1 {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testDisplayDominoes() {
        // Set up your Main class instance
        Main main = new Main();

        // Call the method you want to test
        main.displayDominoes();

        // Define the expected output
        String expectedOutput = ""; // Provide the expected output here

        // Assert the printed output matches the expected output
        assertEquals(expectedOutput, outContent.toString().trim());
    }
}
