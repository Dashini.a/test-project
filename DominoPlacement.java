package base;

public class DominoPlacement {
    public int hx;
    public int hy;
    public int lx;
    public int ly;

    public DominoPlacement(int hx, int hy, int lx, int ly) {
        this.hx = hx;
        this.hy = hy;
        this.lx = lx;
        this.ly = ly;
    }
}

