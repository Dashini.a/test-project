	package base;
	import java.awt.Color;
	import java.awt.Graphics;
	import java.io.BufferedReader;
	import java.io.InputStreamReader;
	
	/**
	 * @author Kevan Buckley, maintained by __student
	 * @version 2.0, 2014
	 */
	
	public class Location extends SpacePlace {
	  public int c;
	  public int r;
	  public DIRECTION d;
	  public int tmp;
	  public enum DIRECTION {VERTICAL, HORIZONTAL};
	  
	  public Location(int r, int c) {
	    this.r = r;
	    this.c = c;
	  }
	
	  public Location(int r, int c, DIRECTION d) {    
	    this(r,c);
	    this.d=d;
	  }
	  
	  public String toString() {
	    if(d==null){
	      tmp = c + 1;
	      return "(" + (tmp) + "," + (r+1) + ")";
	    } else {
	      tmp = c + 1;
	      return "(" + (tmp) + "," + (r+1) + "," + d + ")";
	    }
	  }
	  
	  public void drawGridLines(Graphics g) {
		    drawHorizontalLines(g);
		    drawVerticalLines(g);
		}

		private void drawHorizontalLines(Graphics g) {
		    g.setColor(Color.LIGHT_GRAY);
		    for (int tmp = 0; tmp <= 7; tmp++) {
		        int yCoordinate = 20 + tmp * 20;
		        g.drawLine(20, yCoordinate, 180, yCoordinate);
		    }
		}

		private void drawVerticalLines(Graphics g) {
		    g.setColor(Color.LIGHT_GRAY);
		    for (int see = 0; see <= 8; see++) {
		        int xCoordinate = 20 + see * 20;
		        g.drawLine(xCoordinate, 20, xCoordinate, 160);
		    }
		}

	  
	  public static int getInt() {
	    BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
	    do {
	      try {
	        return Integer.parseInt(r.readLine());
	      } catch (Exception e) {
	      }
	    } while (true);
	  }
	}
